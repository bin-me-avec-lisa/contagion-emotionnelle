# Contagion émotionnelle

## Description
Projet de 2ème année d'école d'ingénieur en informatique ISIMA (niveau M1), réalisé par Clara Lacourarie et Lisa Georges sous la supervision de Jean-Marie Favreau et Jérémy Kalsron.
Il est à noter qu'il s'agit d'un projet de 60h, merci pour votre indulgence !

## Comment utiliser ce code ?
 Vous aurez besoin :
 - d'une installation python afin d'exécuter des scripts créant des fichiers CSV pour stocker les données sur les agents.
 - de tout logiciel de bureautique supportant le format CSV (tableur)
 - d'installer le logiciel GAMA https://gama-platform.org/ 
 
 Vous pouvez trouver :
 - dans le dossier "includes", certains fichiers CSV générés pour effectuer des simulations et des scripts pythons pour remplir les salles de classes selon plusieurs critères (paramètres fixés ou distribués selon une gaussienne)
 - dans "models", un fichier.gaml qui vous permettra de lancer directement une simulation. Vous pouvez changer tout au début du code le chemin de fichier afin de charger un autre fichier CSV.
 Vous pouvez également tester avec des valeurs différentes pour les distances maximales (pour notre travail, nous le fixions à 180 pour les classes en îlots afin
 d'observer le phénomène de clustering et pour les autres nous pouvions les mettre à 200 par exemple pour augmenter le vitesse de convergence).
 
