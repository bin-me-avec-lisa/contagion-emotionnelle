
//   /!\ penser à passer la distance à la ligne 96 à 180 pour les ilots, sinon c'est 200/!\
// si il y a un prof : decommenter ligne avec afficher le 31e agent


model bimodale


global {
	
	
	
	
	file Fichier_eleves <- csv_file("../includes/scripts_python/Uprofdatamoy.csv", ";");
	file Fichier_alpha <- csv_file("../includes/scripts_python/Uprofalpha.csv", ";");
	file Fichier_omega <- csv_file("../includes/scripts_python/Uprofomega.csv", ";");
	
	matrix omega ; 
	matrix eleves ; 
	list<people> liste_agents;
	
	int longueur <- 1000 parameter: "long"; 
	int largeur <- 800 parameter: "larg";
	geometry shape <- rectangle(largeur#m, longueur#m);
	
		

	init 
	{
		matrix alpha <- matrix(Fichier_alpha);
		float abs;
		float ord;
		
		eleves <- matrix(Fichier_eleves);
		omega <-matrix(Fichier_omega);
		//write omega;
		alpha[0,0] <- 1.0;
		
		
		
			
		//write alpha;
		
		loop nb from: 0 to: eleves.rows-1 
		{	
			if (eleves[5,nb] as bool)
			{
				create people
				{
					abs <- eleves[0,nb] as int;
					ord <- eleves[1,nb] as int;
					
					location <- {abs,ord};
					delta <- eleves[2,nb];
					eta <- eleves[3,nb] ;
					//write eta;
					humeur <- eleves[4,nb];
					est_prof<-1;
					
					id <- nb;
				}
			}
			else 
			{
				create people 
				{
					abs <- eleves[0,nb] as int;
					ord <- eleves[1,nb] as int;
					
					location <- {abs,ord} ;
					delta <- eleves[2,nb];
					eta <- eleves[3,nb] ;
					//write eta;
					humeur <- eleves[4,nb];
					id <- nb;
					est_prof<-0;
				}
			}
		}
		liste_agents <- list<people>(people);
		
		//maj des alpha pour prendre en compte la distance
		loop em over: liste_agents 
		{
			loop re over: liste_agents 
			{
				if (sqrt((em.location.x-re.location.x)^2+(em.location.y-re.location.y)^2)>=200 and (re.est_prof=false and em.est_prof=false))
				{
					alpha[em.id,re.id]<-0.000001;
					alpha[re.id,em.id]<-0.000001;
						
				}
				
				if (re.est_prof)
				{

					if (sqrt((em.location.x-re.location.x)^2+(em.location.y-re.location.y)^2)>=150 )
					{
						float dist <- sqrt((em.location.x-re.location.x)^2+(em.location.y-re.location.y)^2);
						
			
						float alphaer <- alpha[em.id,re.id];
						float alphare <- alpha[re.id,em.id]; 
						alpha[em.id,re.id]<- alphaer*(-0.0012*dist+1.18) ;
						alpha[re.id,em.id] <- alphare*(-0.0012*dist+1.18) ;
						
					}
				}
				
					
			}
		
		}
			write alpha;
		
		//calcul de gamma
		float somme ;
		float alphaer ;
		loop receveur over: liste_agents 
		{
			somme <- 0.0;
			loop emetteur over: liste_agents
			{
				if !(receveur.id=emetteur.id) 
				{
			
					alphaer <- alpha[receveur.id,emetteur.id] ;
					somme <- somme + emetteur.eta*alphaer*receveur.delta;	
				}	
			}
			receveur.gamma <- somme; 
		}
		float alphaer ;
		loop receveur over: liste_agents 
		{
			somme <- 0.0;
			loop emetteur over: liste_agents 
			{
				if !(receveur.id=emetteur.id) 
				{	alphaer <- alpha[receveur.id,emetteur.id];
					somme <- somme + emetteur.eta*alphaer;	
				}			
			}	
			loop emetteur over: liste_agents 
			{
				
				if !(receveur.id=emetteur.id) 
				{
					alphaer <- alpha[receveur.id,emetteur.id];
					omega[emetteur.id,receveur.id] <- alphaer*emetteur.eta/somme;
					
				}
			}	
		}
	}
}



species people {
	//attributs location.x et location.y natifs
	float delta; //capacité recevoir
	float eta; //capacité émettre
	float humeur ;
	float ancien_humeur ;
	float gamma;
	int id ;
	bool est_prof;
	
	rgb color ;
	
	
	aspect default {
	  draw circle(25) color: color ;	  
	  
	  
	
	}
	reflex couleur 
	{
		if (humeur < 0) 
		{
			color <- rgb(255, 255*(1+humeur) as int ,0);
		}
		else 
		{
			color <- rgb(255-255*humeur as int , 255, 0);
		}
	}
	reflex evolution_humeur
	{
		float moy_qr <- 0.0;
		ancien_humeur <- humeur;
		write(omega) ;
		loop i from:0 to:eleves.rows-1 
		{
			people emetteur <- liste_agents[i] ;
	
			
			write "je suis " +id + " et on étudit l'influence sur moi de " +emetteur.id ;
			float tmp <- omega[emetteur.id, id];
				
			if (id<emetteur.id)
			{
				moy_qr <- moy_qr + emetteur.humeur * tmp ;
				write "humeur de "+ emetteur.id  + " " + emetteur.humeur ;
				write tmp;
			}
			else 
			{
				if (id>emetteur.id) 
				{
					moy_qr <- moy_qr + emetteur.ancien_humeur * tmp ;
					write "ancienne humeur de "+ emetteur.id  + " " + emetteur.ancien_humeur ;
					write tmp;
				}	
			}			
							
		}
		float var1 <- gamma * (moy_qr - humeur);
		write "q av " + id + " "+ humeur ;
		write " " + id + " "+ "moy" + moy_qr;
		humeur <- humeur + 0.1*var1;
		write "q ap" + id  + " " + humeur ;
	}
	
}





experiment test  type: gui {

  output {
    display View1 type: opengl {
      species people;
      
    }
    display my_display {
    	 chart "my_chart" type: series {
    	 	loop i from:0 to:eleves.rows-1 
		{
            data "0" value: liste_agents[0].humeur color: #red;}
            data "1" value: liste_agents[1].humeur  color: #blue;
            data "2" value: liste_agents[2].humeur  color: #green;
            data "3" value: liste_agents[3].humeur color: #red;
            data "4" value: liste_agents[4].humeur  color: #blue;
            data "5" value: liste_agents[5].humeur  color: #green;
            data "6" value: liste_agents[6].humeur  color: #blue;
            data "7" value: liste_agents[7].humeur  color: #green;
            data "8" value: liste_agents[8].humeur  color: #blue;
            data "9" value: liste_agents[9].humeur  color: #green;
            data "10" value: liste_agents[10].humeur color: #red;
            data "11" value: liste_agents[11].humeur  color: #blue;
            data "12" value: liste_agents[12].humeur  color: #green;
            data "13" value: liste_agents[13].humeur color: #red;
            data "14" value: liste_agents[14].humeur  color: #blue;
            data "15" value: liste_agents[15].humeur  color: #green;
            data "16" value: liste_agents[16].humeur  color: #blue;
            data "17" value: liste_agents[17].humeur  color: #green;
            data "18" value: liste_agents[18].humeur  color: #blue;
            data "19" value: liste_agents[19].humeur  color: #green;
            data "20" value: liste_agents[20].humeur color: #red;
            data "21" value: liste_agents[21].humeur  color: #blue;
            data "22" value: liste_agents[22].humeur  color: #green;
            data "23" value: liste_agents[23].humeur color: #red;
            data "24" value: liste_agents[24].humeur  color: #blue;
            data "25" value: liste_agents[25].humeur  color: #green;
            data "26" value: liste_agents[26].humeur  color: #blue;
            data "27" value: liste_agents[27].humeur  color: #green;
            data "28" value: liste_agents[28].humeur  color: #blue;
            data "29" value: liste_agents[29].humeur  color: #green;
            data "30" value: liste_agents[30].humeur  color: #purple;
            }
    }
  }
	
}
